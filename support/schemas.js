function getSchemaUsuarioLogar(){
    const schema = {
        "type": "object",
        "required": [
            "nome",
            "logou"
        ],
        "properties": {
            "nome": {
                "$id": "#/properties/nome",
                "type": "string"
            },
            "logou": {
                "$id": "#/properties/logou",
                "type": "boolean"
            }
        }
    }
    return schema
}

function getSchemaUsuarioContextoMudar(){
    const schema = {
        "type": "object",
        "required": [
            "currentOwner",
            "owners",
            "nome",
            "permissoes",
            "todasAsPemissoes"
        ],
        "properties": {
            "currentOwner": {
                "$id": "#/properties/currentOwner",
                "type": "integer"
            },
            "owners": {
                "$id": "#/properties/owners",
                "type": "array",
                "items": {
                    "$id": "#/properties/owners/items",
                    "anyOf": [
                        {
                            "$id": "#/properties/owners/items/anyOf/0",
                            "type": "object",
                            "required": [
                                "id",
                                "nome"
                            ],
                            "properties": {
                                "id": {
                                    "$id": "#/properties/owners/items/anyOf/0/properties/id",
                                    "type": "integer"
                                },
                                "nome": {
                                    "$id": "#/properties/owners/items/anyOf/0/properties/nome",
                                    "type": "string"
                                }
                            }
                        }
                    ]
                }
            },
            "nome": {
                "$id": "#/properties/nome",
                "type": "string"
            },
            "permissoes": {
                "$id": "#/properties/permissoes",
                "type": "array",
                "items": {
                    "$id": "#/properties/permissoes/items"
                }
            },
            "todasAsPemissoes": {
                "$id": "#/properties/todasAsPemissoes",
                "type": "object"
            }
        }
    }

    return schema
}

function getSchemaNegativoSemBodyUsuario(){
    const schema = {
        "type": "object",
        "required": [
            "mensagem",
            "codigoRetorno"
        ],
        "properties": {
            "mensagem": {
                "$id": "#/properties/mensagem",
                "type": "string"
            },
            "codigoRetorno": {
                "$id": "#/properties/codigoRetorno",
                "type": "null"
            }
        }
    }
    return schema
}

function getSchemaNegativoUsuarioLogar(){
    const schema = {
        "type": "object",
        "required": [
            "nome",
            "logou"
        ],
        "properties": {
            "nome": {
                "$id": "#/properties/nome",
                "type": "null"
            },
            "logou": {
                "$id": "#/properties/logou",
                "type": "boolean"
            }
        }
    }

    return schema
}

function getSchemaNotFound(){
    const schema = {
        "type": "object",
        "required": [
            "timestamp",
            "status",
            "error",
            "message",
            "path"
        ],
        "properties": {
            "timestamp": {
                "$id": "#/properties/timestamp",
                "type": "integer"
            },
            "status": {
                "$id": "#/properties/status",
                "type": "integer"
            },
            "error": {
                "$id": "#/properties/error",
                "type": "string"
            },
            "message": {
                "$id": "#/properties/message",
                "type": "string"
            },
            "path": {
                "$id": "#/properties/path",
                "type": "string"
            }
        }
    }

    return schema
}

function getSchemaNegativoNotFoundUsuario(){
    const schema = {
        "type": "object",
        "required": [
            "mensagem",
            "codigoRetorno"
        ],
        "properties": {
            "mensagem": {
                "$id": "#/properties/mensagem",
                "type": "string"
            },
            "codigoRetorno": {
                "$id": "#/properties/codigoRetorno",
                "type": "integer"
            }
        }
    }
    return schema
}

function getSchemaUsuarioLogado(){
    const schema = {
        "type": "object",
        "required": [
            "permissoes",
            "todasAsPemissoes"
        ],
        "properties": {
            "permissoes": {
                "$id": "#/properties/permissoes",
                "type": "array",
                "items": {
                    "$id": "#/properties/permissoes/items"
                }
            },
            "todasAsPemissoes": {
                "$id": "#/properties/todasAsPemissoes",
                "type": "object"
            }
        }
    }
    return schema
}

function getSchemaUsuarioContexto(){
    const schema = {
        "type": "object",
        "required": [
            "currentOwner",
            "owners",
            "nome",
            "permissoes",
            "todasAsPemissoes"
        ],
        "properties": {
            "currentOwner": {
                "$id": "#/properties/currentOwner",
                "type": "integer"
            },
            "owners": {
                "$id": "#/properties/owners",
                "type": "array",
                "items": {
                    "$id": "#/properties/owners/items",
                    "anyOf": [
                        {
                            "$id": "#/properties/owners/items/anyOf/0",
                            "type": "object",
                            "required": [
                                "id",
                                "nome"
                            ],
                            "properties": {
                                "id": {
                                    "$id": "#/properties/owners/items/anyOf/0/properties/id",
                                    "type": "integer"
                                },
                                "nome": {
                                    "$id": "#/properties/owners/items/anyOf/0/properties/nome",
                                    "type": "string"
                                }
                            }
                        }
                    ]
                }
            },
            "nome": {
                "$id": "#/properties/nome",
                "type": "string"
            },
            "permissoes": {
                "$id": "#/properties/permissoes",
                "type": "array",
                "items": {
                    "$id": "#/properties/permissoes/items"
                }
            },
            "todasAsPemissoes": {
                "$id": "#/properties/todasAsPemissoes",
                "type": "object"
            }
        }
    }
    return schema
}
function getConsultarStatus(){
    const schema = {

        
            "type": "array",
            "items": {
                "$id": "#/items",
                "type": "object",
                "required": [
                    "codigoBacen",
                    "nome",
                    "ispb",
                    "emailSoliScg",
                    "numOperacaoDomBanco"
                ],
                "properties": {
                    "codigoBacen": {
                        "$id": "#/items/properties/codigoBacen",
                        "type": "integer"
                    },
                    "nome": {
                        "$id": "#/items/properties/nome",
                        "type": "string"
                    },
                    "ispb": {
                        "$id": "#/items/properties/ispb",
                        "type": "integer"
                    },
                    "emailSoliScg": {
                        "$id": "#/items/properties/emailSoliScg",
                        "type": ["string","null"]
                    },
                    "numOperacaoDomBanco": {
                        "$id": "#/items/properties/numOperacaoDomBanco",
                        "type": "boolean"
                    }
            }
        }
        
    }
    return schema
}


function getSchemaNegativoInvalidUserToken (){
    const schema = {
        "type": "object",
        "required": [
            "mensagem",
            "codigoRetorno"
        ],
        "properties": {
            "mensagem": {
                "$id": "#/properties/mensagem",
                "type": "string"
            },
            "codigoRetorno": {
                "$id": "#/properties/codigoRetorno",
                "type": "null"
            }
        },
        "additionalProperties": true
    }
    return schema
}

function getAnuenciasStatus(){
    const schema = {
        
        "default": [],
        "type": "array",
        "items": {
            "$id": "#/items",
            "anyOf": [
                {
                    "$id": "#/items/anyOf/0",
                    "type": "object",
                    "default": {},
                    "required": [
                        "canalSolicitante",
                        "codArranjo",
                        "codSituacao",
                        "dataHoraOptin",
                        "dataHoraOptout",
                        "descSituacao",
                        "idOperacaoOptin",
                        "idOperacaoOptout",
                        "numDocumento",
                        "tipoDocumento"
                    ],
                    "properties": {
                        "canalSolicitante": {
                            "$id": "#/items/anyOf/0/properties/canalSolicitante",
                            "type": "string",
                            "default": ""
                        },
                        "codArranjo": {
                            "$id": "#/items/anyOf/0/properties/codArranjo",
                            "type": "string",
                            "default": ""
                        },
                        "codSituacao": {
                            "$id": "#/items/anyOf/0/properties/codSituacao",
                            "type": "integer",
                            "default": 0
                        },
                        "dataHoraOptin": {
                            "$id": "#/items/anyOf/0/properties/dataHoraOptin",
                            "type": "string",
                            "default": ""
                        },
                        "dataHoraOptout": {
                            "$id": "#/items/anyOf/0/properties/dataHoraOptout",
                            "type": "string",
                            "default": ""
                        },
                        "descSituacao": {
                            "$id": "#/items/anyOf/0/properties/descSituacao",
                            "type": "string",
                            "default": ""
                        },
                        "idOperacaoOptin": {
                            "$id": "#/items/anyOf/0/properties/idOperacaoOptin",
                            "type": "string",
                            "default": ""
                        },
                        "idOperacaoOptout": {
                            "$id": "#/items/anyOf/0/properties/idOperacaoOptout",
                            "type": "string",
                            "default": ""
                        },
                        "numDocumento": {
                            "$id": "#/items/anyOf/0/properties/numDocumento",
                            "type": "integer",
                            "default": 0
                        },
                        "tipoDocumento": {
                            "$id": "#/items/anyOf/0/properties/tipoDocumento",
                            "type": "string",
                            "default": ""
                        }
                    }
                }
            ]
        }
    } 
    return schema
}


function getAnuenciasStatusBadRequest (){
    const schema ={

       
            "type": "object",
            "required": [
                "mensagem",
                "codigoRetorno"
            ],
            "properties": {
                "mensagem": {
                    "$id": "#/properties/mensagem",
                    "type": "string"
                },
                "codigoRetorno": {
                    "$id": "#/properties/codigoRetorno",
                    "type": "null"
                }
            }
        
    } 
    return schema
}
function getRetornaDomiciliosBancarios (){
    const schema ={
        
            "type": "array",
            "items": {
                "$id": "#/items",
                "anyOf": [
                    {
                        "$id": "#/items/anyOf/0",
                        "type": "object",
                        "required": [
                            "idEstabelecimentoCentral",
                            "idDomicilioEstabelecimento",
                            "agencia",
                            "codigoConta",
                            "digitoConta",
                            "digitoAgencia",
                            "tipoConta",
                            "codigoBacen",
                            "nome",
                            "ispb",
                            "codArranjo",
                            "descArranjo",
                            "codigoProduto",
                            "descTipoConta"
                        ],
                        "properties": {
                            "idEstabelecimentoCentral": {
                                "$id": "#/items/anyOf/0/properties/idEstabelecimentoCentral",
                                "type": "null"
                            },
                            "idDomicilioEstabelecimento": {
                                "$id": "#/items/anyOf/0/properties/idDomicilioEstabelecimento",
                                "type": "integer"
                            },
                            "agencia": {
                                "$id": "#/items/anyOf/0/properties/agencia",
                                "type": "integer"
                            },
                            "codigoConta": {
                                "$id": "#/items/anyOf/0/properties/codigoConta",
                                "type": "integer"
                            },
                            "digitoConta": {
                                "$id": "#/items/anyOf/0/properties/digitoConta",
                                "type": "string"
                            },
                            "digitoAgencia": {
                                "$id": "#/items/anyOf/0/properties/digitoAgencia",
                                "type": "null"
                            },
                            "tipoConta": {
                                "$id": "#/items/anyOf/0/properties/tipoConta",
                                "type": "string"
                            },
                            "codigoBacen": {
                                "$id": "#/items/anyOf/0/properties/codigoBacen",
                                "type": "integer"
                            },
                            "nome": {
                                "$id": "#/items/anyOf/0/properties/nome",
                                "type": "string"
                            },
                            "ispb": {
                                "$id": "#/items/anyOf/0/properties/ispb",
                                "type": "integer"
                            },
                            "codArranjo": {
                                "$id": "#/items/anyOf/0/properties/codArranjo",
                                "type": "integer"
                            },
                            "descArranjo": {
                                "$id": "#/items/anyOf/0/properties/descArranjo",
                                "type": "string"
                            },
                            "codigoProduto": {
                                "$id": "#/items/anyOf/0/properties/codigoProduto",
                                "type": "integer"
                            },
                            "descTipoConta": {
                                "$id": "#/items/anyOf/0/properties/descTipoConta",
                                "type": "string"
                            }
                        }
                    }
                ]
            }
        }
    
    return schema
}

function getRetornaEstabelecimentoById (){
    const schema = {
      
            "type": "object",
            "required": [
                "idEstabelecimento",
                "codigoEstabelecimento",
                "razaoSocial",
                "usuario",
                "dataAtualizacao",
                "documento",
                "raizCnpj",
                "situacao",
                "tipoPessoa",
                "idEstabelecimentoCentral",
                "codComercio",
                "tipoPlataforma",
                "codigoOrigem",
                "dataEnvioAlteracaoCIP",
                "codigoRetCip",
                "situacaoEnvioCip",
                "descOrigem"
            ],
            "properties": {
                "idEstabelecimento": {
                    "$id": "#/properties/idEstabelecimento",
                    "type": "integer"
                },
                "codigoEstabelecimento": {
                    "$id": "#/properties/codigoEstabelecimento",
                    "type": "integer"
                },
                "razaoSocial": {
                    "$id": "#/properties/razaoSocial",
                    "type": "string"
                },
                "usuario": {
                    "$id": "#/properties/usuario",
                    "type": "string"
                },
                "dataAtualizacao": {
                    "$id": "#/properties/dataAtualizacao",
                    "type": "string"
                },
                "documento": {
                    "$id": "#/properties/documento",
                    "type": "string"
                },
                "raizCnpj": {
                    "$id": "#/properties/raizCnpj",
                    "type": "integer"
                },
                "situacao": {
                    "$id": "#/properties/situacao",
                    "type": "string"
                },
                "tipoPessoa": {
                    "$id": "#/properties/tipoPessoa",
                    "type": "string"
                },
                "idEstabelecimentoCentral": {
                    "$id": "#/properties/idEstabelecimentoCentral",
                    "type": "null"
                },
                "codComercio": {
                    "$id": "#/properties/codComercio",
                    "type": "string"
                },
                "tipoPlataforma": {
                    "$id": "#/properties/tipoPlataforma",
                    "type": "string"
                },
                "codigoOrigem": {
                    "$id": "#/properties/codigoOrigem",
                    "type": "string"
                },
                "dataEnvioAlteracaoCIP": {
                    "$id": "#/properties/dataEnvioAlteracaoCIP",
                    "type": "string"
                },
                "codigoRetCip": {
                    "$id": "#/properties/codigoRetCip",
                    "type": "null"
                },
                "situacaoEnvioCip": {
                    "$id": "#/properties/situacaoEnvioCip",
                    "type": "string"
                },
                "descOrigem": {
                    "$id": "#/properties/descOrigem",
                    "type": "string"
                }
            }
        }
        
    
    
    return schema
}

function getConsultarEstabelecimento (){
    const schema ={
        
            "type": "object",
            "required": [
                "itens",
                "totais",
                "qtdPorPagina",
                "paginaAtual",
                "totalPaginas"
            ],
            "properties": {
                "itens": {
                    "$id": "#/properties/itens",
                    "type": "array",
                    "items": {
                        "$id": "#/properties/itens/items",
                        "anyOf": [
                            {
                                "$id": "#/properties/itens/items/anyOf/0",
                                "type": "object",
                                "required": [
                                    "idEstabelecimento",
                                    "codigoEstabelecimento",
                                    "razaoSocial",
                                    "usuario",
                                    "dataAtualizacao",
                                    "documento",
                                    "raizCnpj",
                                    "situacao",
                                    "tipoPessoa",
                                    "idEstabelecimentoCentral",
                                    "codComercio",
                                    "tipoPlataforma",
                                    "codigoOrigem",
                                    "dataEnvioAlteracaoCIP",
                                    "codigoRetCip",
                                    "situacaoEnvioCip",
                                    "descOrigem"
                                ],
                                "properties": {
                                    "idEstabelecimento": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/idEstabelecimento",
                                        "type": "integer"
                                    },
                                    "codigoEstabelecimento": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/codigoEstabelecimento",
                                        "type": "integer"
                                    },
                                    "razaoSocial": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/razaoSocial",
                                        "type": "string"
                                    },
                                    "usuario": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/usuario",
                                        "type": "string"
                                    },
                                    "dataAtualizacao": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/dataAtualizacao",
                                        "type": "string"
                                    },
                                    "documento": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/documento",
                                        "type": "string"
                                    },
                                    "raizCnpj": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/raizCnpj",
                                        "type": "integer"
                                    },
                                    "situacao": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/situacao",
                                        "type": "string"
                                    },
                                    "tipoPessoa": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/tipoPessoa",
                                        "type": "string"
                                    },
                                    "idEstabelecimentoCentral": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/idEstabelecimentoCentral",
                                        "type": "null"
                                    },
                                    "codComercio": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/codComercio",
                                        "type": "string"
                                    },
                                    "tipoPlataforma": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/tipoPlataforma",
                                        "type": "string"
                                    },
                                    "codigoOrigem": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/codigoOrigem",
                                        "type": "string"
                                    },
                                    "dataEnvioAlteracaoCIP": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/dataEnvioAlteracaoCIP",
                                        "type": "string"
                                    },
                                    "codigoRetCip": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/codigoRetCip",
                                        "type": "null"
                                    },
                                    "situacaoEnvioCip": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/situacaoEnvioCip",
                                        "type": "string"
                                    },
                                    "descOrigem": {
                                        "$id": "#/properties/itens/items/anyOf/0/properties/descOrigem",
                                        "type": "string"
                                    }
                                }
                            }
                        ]
                    }
                },
                "totais": {
                    "$id": "#/properties/totais",
                    "type": "integer"
                },
                "qtdPorPagina": {
                    "$id": "#/properties/qtdPorPagina",
                    "type": "integer"
                },
                "paginaAtual": {
                    "$id": "#/properties/paginaAtual",
                    "type": "integer"
                },
                "totalPaginas": {
                    "$id": "#/properties/totalPaginas",
                    "type": "integer"
                }
            }
        
    }
    return schema
}