usuario_logar = (response, logou, usuario) => JSON.stringify({
    'nome': {
        'path': response.nome,
        'expect': usuario
    },
    'logou': {
        'path': response.logou,
        'expect': logou
    }
})

error_message = (response, mensagem, codigoRetorno) => JSON.stringify({
    'mensagem': {
        'path': response.mensagem,
        'expect': mensagem,
    },
    'codigoRetorno': {
        'path': response.codigoRetorno,
        'expect': codigoRetorno
    }
})

not_found = (response, status, error, message, path) => JSON.stringify({
    'status': {
        'path': response.status,
        'expect': status
    },
    'error': {
        'path': response.error,
        'expect': error
    },
    'message': {
        'path': response.message,
        'expect': message
    },
    'path': {
        'path': response.path,
        'expect': path
    }
})



estabelecimento_porId = (response,idEc)=>JSON.stringify({
    "idEstabelecimento":{ 
        "path":response.idEstabelecimento,
        "expect":idEc
    }
})
