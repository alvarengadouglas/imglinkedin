geraUserTokenGetnetPnr = () => {
    let decryptPass = Buffer.from('PzFkRjJScG42bg==', 'base64').toString()
    const options = {
        method: 'POST',
        url: 'https://plataforma-negociacao-api-hti.getnet.com.br/usuario/logar',
        header: {
            'content-type': 'application/json'
        },
        body: {
            mode: 'raw',
            raw: JSON.stringify({
                "senha": `${decryptPass}`,
                "usuario": "svc_hml"
            })
        }
    }
    
    pm.sendRequest(options, (err, res) => {
                if (err) {
                    console.log(err);
                } else {
                    let token = res.headers.get('user_token_getnet_pnr')
                    pm.environment.set('user_token_getnet_pnr', token)
                    console.log("Salvou o user_token_getnet_pnr no environment")
                }
    })
}
